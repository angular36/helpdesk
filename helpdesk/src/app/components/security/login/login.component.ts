import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from 'src/app/model/user.model';
import { SharedService } from 'src/app/services/shared.service';
import { UserService } from 'src/app/services/user/user.service';
import { CurrentUser } from 'src/app/model/current.user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = new User("", "", "", "");
  shared: SharedService;
  message: {};
  classCss: {};

  constructor(private userService: UserService, private router: Router) {
    this.shared = SharedService.getInstance();
  }

  ngOnInit() {
  }

  login() {
    this.message = "";
    this.userService.login(this.user).subscribe((userAuthentication: CurrentUser) => {
      this.shared.token = userAuthentication.token;
      this.shared.user = userAuthentication.user;
      this.shared.user.profile = this.shared.user.profile.substring(5);
      this.shared.showTemplate.emit(true); // Aviso que estou logado para renderizar os templates com [hidden] do app.component.html
      this.router.navigate(["/"]); // Depois de logado, redireciono para "/", no caso, como definido as rotas em app.route.ts, vou para home
    }, erro => {
      this.shared.token = null;
      this.shared.user = null;
      this.shared.showTemplate.emit(false);
      this.showMessage({
        type: 'error',
        text: `Email ou senha inválidos!`
      });
    });
  }

  cancelLogin() {
    this.message = "";
    this.user = new User("", "", "", "");
    window.location.href = "/login";
    window.location.reload(); // Atualizo e limpo tudo 
  }

  getFormGroupClass(isInvalid: boolean, isDirty): {} {
    return {
      "form-group": true,
      "has-error": isInvalid && isDirty,
      "has-success": !isInvalid && isDirty
    };
  }

  private showMessage(message: { type: string, text: string }): void {
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    }, 3000);
  }

  private buildClasses(type: string): void {
    this.classCss = {
      'alert': true
    }
    this.classCss['alert-' + type] = true;
  }

}
