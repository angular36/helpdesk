import { Injectable, EventEmitter } from '@angular/core';

import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public static instance: SharedService = null;
  user: User;
  token: string;
  showTemplate = new EventEmitter<boolean>(); // Esse atributo significa que quando o usuário estiver logado, todos os templates declarados em app.component.html serão renderizados

  constructor() { 
    return SharedService.instance = SharedService.instance || this;
  }

  public static getInstance() {
    if(this.instance == null) {
      this.instance = new SharedService();
    }
    return this.instance;
  }

  isLoggedIn(): boolean {
    if(this.user == null) {
      return false; // Não está logado
    }
    return this.user.email != '';
  }
}
