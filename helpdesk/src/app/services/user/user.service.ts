import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';

import { User } from 'src/app/model/user.model';
import { HELP_DESK_API } from '../helpdesk.api';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  login(user: User) { // Login
    return this.http.post(`${HELP_DESK_API}/api/auth`, user); // Uso post porque na API o endpoint está @PostMapping
  }

  createOrUpdate(user: User){ // Crio ou altero usuario
    if(user.id != null && user.id != '') {
      return this.http.put(`${HELP_DESK_API}/api/user`, user);
    } else {
      user.id = null;
      return this.http.post(`${HELP_DESK_API}/api/user`, user);
    }
  }

  findAll(page: number, count: number) { // Listo todos usuarios, o page e count são parâmetros que vão como parte da url "/api/user/1/10" 
    return this.http.get(`${HELP_DESK_API}/api/user/${page}/${count}`);
  }

  findById(id: string) { // Procuro um usuario pelo id
    return this.http.get(`${HELP_DESK_API}/api/user/${id}`);
  }

  delete(id: string) { // Deleto um usuario pelo id
    return this.http.delete(`${HELP_DESK_API}/api/user/${id}`);
  }


}
